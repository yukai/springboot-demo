package com.springboot.webdemo.dao;




import com.springboot.webdemo.core.result.Pager;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;


/**
 * @Author: 余凯
 * @Description: 底层基本的dao的接口
 * @Date: 2017/12/22
 * @Modified by：
 */
public interface BaseDao<T, PK extends Serializable> { 
  
    /** 
     * 保存单一对象，如果要保存多个对象集合，请参看{@link #insertList(List)} 
     *  
     * @param entity 
     */
    public T insert(T entity); 
      
    /** 
     * 保存指定类型的对象集合,如果要保存单一对象，请参看{@link #insert(Object)} 
     * @param entities 要保存的数据集合 
     * @return 
     */
    public List<T> insertList(List<T> entities); 
  
    /** 
     * 更新对象,如果属性为空，则不会进行对应的属性值更新,如果有属性要更新为null
     *  
     * @param entity 
     *            要更新的实体对象 
     */
    public void update(T entity);

    void updateList(List<T> entities);

    void delete();

    /**
     * 根据id删除对象 
     *  
     * @param id 
     */
    public void deleteById(PK id); 
  
    /** 
     * 根据条件集合删除对象 
     *  
     * @param condition
     */
    public void deleteByCondition(Map<String, Object> condition);


    /** 
     * 批量删除对象 
     *  
     * @param entities
     */
    public void deleteList(List<Integer> entities);



    void deleteBatch(List<T> entities);

    /**
	 * 批量删除
	 * @param ids id数组
	 * @return 删除的记录数 
	 */
	int deleteByIds(PK[] ids);

    /**
     * 根据id进行对象查询 
     *  
     * @param id 
     * @return 
     */
    public T fetch(PK id); 
  
    /**
     * 根据传入的泛型参数类型查询该类型对应表中的所有数据，返回一个集合对象 
     *  
     * @return 返回泛型参数类型的对象集合，如何取到泛型类型参数，请参看{@link #getEntityClass()} 
     */
    public List<T> findAll(); 
  
    /** 
     * 根据条件集合进行分页查询 
     *  
     * @param condition 
     *            查询条件 
     * @param currentPage 
     *            当前页数 
     * @param pageSize 
     *            页面大小 
     * @return 返回Pager对象 
     */
    public Pager<T> queryPage(Map<String, Object> condition, Integer currentPage,
                              Integer pageSize);

    Pager<Map<String,Object>> queryPageMap(Map<String, Object> condition, Integer currentPage, Integer pageSize);

    Pager<T> queryPage(String mapperId, Map<String, Object> condition,
                       Integer currentPage, Integer pageSize);

    Pager<Map<String,Object>> queryPageMap(String mapperId, Map<String, Object> condition,
                                           Integer currentPage, Integer pageSize);

    List<Map<String, Object>> queryListMap(Map<String, Object> condition, String orderBy,
                                           String sortBy);

    /**
     * 根据条件集合进行指定类型对象集合查询 
     *  
     * @param condition 
     *            进行查询的条件集合 
     * @return 返回泛型参数类型的对象集合，如何取到泛型类型参数，请参看{@link #getEntityClass()} 
     */
    public List<T> queryList(Map<String, Object> condition, String orderBy, String sortBy);
      
    /** 
     * 根据条件集合进行指定类型单一对象查询 
     *  
     * @param condition 
     *            进行查询的条件集合 
     * @return 返回泛型参数类型的对象，如何取到泛型类型参数，请参看{@link #getEntityClass()}， 
     */
    public T queryOne(Map<String, Object> condition); 
  
    /** 
     * 根据条件进行数量的查询 
     *  
     * @param condition 
     * @return 返回符合条件的泛型参数对应表中的数量 
     */
    public int count(Map<String, Object> condition); 
  
    /**
     * 更新或保存，涉及到Mabatis使用的bean只是一个简单的值对象，不能进行id的注解，不知道哪个是主键，所以，必须同时指定t的主键值 
     *  
     * @param t 
     *            要更新或保存的对象 
     * @param id 
     *            要更新或保存的对象的id 
     * @return 返回更新或保存的对象。 
     * @throws NoSuchMethodException  
     * @throws InvocationTargetException  
     * @throws IllegalAccessException  
     * @throws SecurityException  
     * @throws IllegalArgumentException  
     */
    public T updateOrSave(T t, PK id); 

    /**
     * 取得泛型类型 
     *  
     * @return 
     */
    public Class<T> getEntityClass();
}