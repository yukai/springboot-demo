package com.springboot.webdemo.dao.permission.impl;


import com.springboot.webdemo.dao.BaseDaoImpl;
import com.springboot.webdemo.dao.permission.IMenuDao;
import com.springboot.webdemo.model.permission.Menu;
import org.springframework.stereotype.Repository;

/**
 * @Author: 余凯
 * @Description: 菜单dao实现
 * @Date: 2017/12/27
 * @Modified by：
 */
@Repository
public class MenuDaoImpl extends BaseDaoImpl<Menu,Integer> implements IMenuDao {
}
