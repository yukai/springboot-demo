package com.springboot.webdemo.dao.permission;


import com.springboot.webdemo.dao.BaseDao;
import com.springboot.webdemo.model.permission.Menu;

/**
 * @Author: 余凯
 * @Description: 菜单管理dao
 * @Date: 2017/12/22
 * @Modified by：
 */
public interface IMenuDao extends BaseDao<Menu,Integer> {

}
