package com.springboot.webdemo.core.enums;

public enum DefaultStatusEnum {
    Normal(1, "正常"), Close(2, "关闭"), Upgrade(3, "升级中");
    String value;
    Integer type;

    DefaultStatusEnum(Integer type, String value) {
        this.value = value;
        this.type = type;
    }

    public static String getValue(int type) {
        DefaultStatusEnum[] enums = DefaultStatusEnum.values();
        if (enums != null && enums.length > 0) {
            for (DefaultStatusEnum em : enums) {
                if (em.type == type) {
                    return em.value;
                }
            }
        }
        return null;
    }

    public int getType() {
        return type;
    }
}
