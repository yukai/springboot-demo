package com.springboot.webdemo.core.constants;

/**
 * @Author: 余凯
 * @Description: 系统的静态变量
 * @Date: 2017/12/22
 * @Modified by：
 */
public class SystemConstant {
    /**系统默认编码*/
    public static final String CHARACTER_ENCODING = "UTF-8";

}
