package com.springboot.webdemo.core.excel;

import com.alibaba.fastjson.JSON;
import com.springboot.webdemo.core.result.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: 余凯
 * @Description: excel操作demo
 * @Date: 2017/12/20
 * @Modified by：
 */
public class ExcelOperDemo {

    /**
     * @Author: 余凯
     * @Description: 设置内容包，需要和表头顺序一直
     * @Date: 2017/12/20
     * @Modified by：
     */
    public List<Map<Integer,Object>> getDataBody(List<Map<String,Object>> dataMap) {
        List<Map<Integer,Object>> body = new ArrayList<Map<Integer, Object>>();
        Map<Integer,Object> bodyItem;
        for(Map<String,Object> data : dataMap) {
            bodyItem = new HashMap<Integer, Object>();
            bodyItem.put(0,data.get("deptName"));
            bodyItem.put(1,data.get("empName"));
            bodyItem.put(2,data.get("dutyName"));
            body.add(bodyItem);
        }
        return body;
    }
    /**
     * @Author: 余凯
     * @Description: 设置表头信息
     * @Date: 2017/12/20
     * @Modified by：
     */
    public Map<Integer,String> getHeadMap() {
        Map<Integer,String> head = new HashMap<Integer, String>();
        head.put(0,"部门名称");
        head.put(1,"员工名称");
        head.put(2,"职位名称");

        return head;
    }
    public static void main(String args[]) {
        //测试导出
        ExcelOperDemo demo = new ExcelOperDemo();
        /*List<Map<String,Object>> dataMap = new ArrayList<Map<String, Object>>();
        dataMap.add(new HashMap<String, Object>(){{this.put("deptName","行政中心");this.put("empName","余凯");this.put("dutyName","Java开发工程师");}});
        ExcelUtil.writeSingleSheet("D://测试导出.xlsx","员工列表",demo.getHeadMap(),demo.getDataBody(dataMap));*/

        //测试导入
        Map<String,String> headMap = new HashMap<String, String>();
        headMap.put("部门名称","deptName");
        headMap.put("员工名称","empName");
        headMap.put("职位名称","dutyName");
        Message msg = ExcelUtil.read("D://测试导出.xlsx",headMap);
        System.out.print(JSON.toJSONString(msg));
    }


}
