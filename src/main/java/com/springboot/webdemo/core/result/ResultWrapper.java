package com.springboot.webdemo.core.result;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;

/**
 * @Author: 余凯
 * @Description: 后台返回给前端的JSON封装类
 *          error_code为0时表示请求成功，为-1时表示失败，其他值自定义
 * @Date: 2017/12/22
 * @Modified by：
 */
public class ResultWrapper {
    private static final Logger log = LoggerFactory.getLogger(ResultWrapper.class);
    private static SerializeConfig mapping = new SerializeConfig();
    private static String ymdhms = "yyyy-MM-dd HH:mm:ss";

    static {
        mapping.put(Date.class,new SimpleDateFormatSerializer(ymdhms));
        mapping.put(java.sql.Date.class,new SimpleDateFormatSerializer("yyyy-MM-dd"));
        mapping.put(Timestamp.class,new SimpleDateFormatSerializer(ymdhms));
//        mapping.put(Time.class,new SimpleDateFormatSerializer(ymdhms));
//        mapping.put(Timestamp.class,new SimpleDateFormatSerializer("HH:mm:ss"));
        mapping.put(Time.class,new SimpleDateFormatSerializer("HH:mm:ss"));
//        mapping.put(Date.class,new SimpleDateFormatSerializer("yyyy-MM-dd"));
//        mapping.put(Timestamp.class,new SimpleDateFormatSerializer("yyyy-MM-dd"));

    }

    public static String printMsg(Message msg) {
        HashMap map = new HashMap();
        if(msg == null || msg.getStatus() == true) {
            map.put("error_code", 0);
            if(msg != null) {
                if(msg.getMsg() != null) {
                    map.put("message", msg.getMsg());
                }
            }else {
                map.put("message","操作成功");
            }
            if(msg.getEntity() != null) {
                map.put("data", msg.getEntity());
            }

        }else {
            map.put("error_code",msg.getErrorCode() == null?-1:msg.getErrorCode());
            map.put("message", msg.getMsg());
            if(msg.getEntity() != null) {
                map.put("data", msg.getEntity());
            }
        }
        return JSON.toJSONString(map, mapping);
    }
    /**
     * 返回成功信息
     * @param message
     * @param resultObj
     * @return
     */
    public static String getSuccessInfo(String message, Object resultObj) {
        HashMap map = new HashMap();
        map.put("error_code", 0);
        map.put("message", message);
        map.put("data", resultObj);
        return JSON.toJSONString(map, mapping);
    }

    /**
     * 返回失败信息
     * @param message
     * @return
     */
    public static String getFailureInfo(String message) {
        HashMap map = new HashMap();
        map.put("error_code", -1);
        map.put("message", message);
        return JSON.toJSONString(map);
    }

    /**
     * 返回结果信息（包括成功和失败，error_code自定义）
     * @param errorCode
     * @param message
     * @param resultObj
     * @return
     */
    public static String getResultInfo(int errorCode,String message,Object resultObj) {
        HashMap map = new HashMap();
        map.put("error_code",errorCode);
        map.put("message", message);
        if (resultObj != null){
            map.put("data", resultObj);
        }

        return JSON.toJSONString(map, mapping);
    }

}
