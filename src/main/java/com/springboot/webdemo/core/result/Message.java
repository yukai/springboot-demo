package com.springboot.webdemo.core.result;

/**
 * @Author: 余凯
 * @Description: 业务接口结果返回集封装
 * @Date: 2017/12/22
 * @Modified by：
 */
public class Message {
    /**
     * 返回状态 false-失败，true-成功
     */
    private Boolean status;
    /**
     * 返回信息
     */
    private String msg;
    /**
     * 返回的实体
     */
    private Object entity;

    private Integer errorCode;//具体的错误异常code,


    public Message(boolean status) {
        this.status = status;
    }
    public Message(boolean status, String msg) {
        this.status = status;
        this.msg = msg;
    }
    public Message(boolean status, String msg, Object entity) {
        this.status = status;
        this.msg = msg;
        this.entity = entity;
    }
    public Message(boolean status, String msg, Integer errorCode) {
        this.status = status;
        this.msg = msg;
        this.errorCode = errorCode;
    }
    public Message(boolean status, String msg, Integer errorCode,Object entity) {
        this.status = status;
        this.msg = msg;
        this.errorCode = errorCode;
        this.entity = entity;
    }
    public Boolean getStatus() {
        return status;
    }
    public void setStatus(Boolean status) {
        this.status = status;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public Object getEntity() {
        return entity;
    }
    public void setEntity(Object entity) {
        this.entity = entity;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }
}
