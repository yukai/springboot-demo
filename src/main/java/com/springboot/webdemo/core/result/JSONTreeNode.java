package com.springboot.webdemo.core.result;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: 余凯
 * @Description: 树形结构结果集返回封装
 * @Date: 2017/12/22
 * @Modified by：
 */
public class JSONTreeNode implements Serializable {
	private Integer id;
	private String text;    //展示文本
	private String label;	//显示名称
	private boolean leaf;   //是否叶子
	private boolean checked; //是否被选中
	private String state;   //是否展开(closed收缩)
	private Integer orderNo;  //排序信息
	private Integer useStatus ;   //是否停用
	private String treeSign;    //编码层次
 	private Integer parentId; // 父节点ID
 	private String url;
	private String priority;
	private String path;
 	//子节点
 	List<JSONTreeNode> children = new ArrayList<JSONTreeNode>();


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public boolean getLeaf() {
		return leaf;
	}
	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getTreeSign() {
		return treeSign;
	}
	public void setTreeSign(String treeSign) {
		this.treeSign = treeSign;
	}
	public List<JSONTreeNode> getChildren() {
		return children;
	}
	public void setChildren(List<JSONTreeNode> children) {
		this.children = children;
	}
	public Integer getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}

	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	public Integer getUseStatus() {
		return useStatus;
	}
	public void setUseStatus(Integer useStatus) {
		this.useStatus = useStatus;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}


	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
