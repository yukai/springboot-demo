package com.springboot.webdemo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements HealthIndicator {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try {
            SpringApplication.run(Application.class, args);
            logger.info("#####  程序启动 -  SUCCESS  #####");
        } catch (Exception e) {
            logger.info("#####  程序启动失败  #####", e);
            System.exit(0);
        }
    }

    /**
     * 在/health接口调用的时候，返回多一个属性："mySpringBootApplication":{"status":"UP","hello":"world"}
     */
    @Override
    public Health health() {
        return Health.up().withDetail("hello", "world").build();
    }
}
