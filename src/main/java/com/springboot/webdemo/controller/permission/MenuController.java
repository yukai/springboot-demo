package com.springboot.webdemo.controller.permission;

import com.springboot.webdemo.core.enums.DefaultStatusEnum;
import com.springboot.webdemo.core.result.JSONTreeNode;
import com.springboot.webdemo.core.result.ResultWrapper;
import com.springboot.webdemo.core.utils.CommonUtil;
import com.springboot.webdemo.model.permission.Menu;
import com.springboot.webdemo.service.permission.IMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @Author: 余凯
 * @Description: 菜单管理控制器
 * @Date: 2017/12/27
 * @Modified by：
 */
@Controller
@RequestMapping(value = "perMenu")
public class MenuController {
    private static final Logger log = LoggerFactory.getLogger(MenuController.class);
    @Autowired
    private IMenuService menuService;
    /**
     * @Author: 余凯
     * @Description: 查询菜单树
     *      perMenu/findMenuTree
     * @Date: 2017/12/27
     * @Modified by：
     */
    @RequestMapping(value = "/findMenuTree", method = RequestMethod.POST)
    @ResponseBody
    public String findMenuTree(HttpServletRequest request, @RequestBody Map<String,Object> condition) {
        try{
            List<JSONTreeNode> result = menuService.findMenuTree(condition);
            return ResultWrapper.getSuccessInfo("查询菜单树", result);
        }catch (Exception e) {
            log.error("查询菜单树异常",e);
        }
        return ResultWrapper.getFailureInfo("查询菜单树异常");
    }
    /**
     * @Author: 余凯
     * @Description: 新增菜单，默认系统Id为1
     * @Date: 2017/12/27
     * @Modified by：
     */
    @RequestMapping(value = "/addMenu", method = RequestMethod.POST)
    @ResponseBody
    public String addMenu(HttpServletRequest request, @RequestBody Menu menu) {
        try {
            if(menu.getSysId() == null) {
                menu.setSysId(1);
            }
            if(CommonUtil.isBlank(menu.getMenuName())) {
                return ResultWrapper.getFailureInfo("菜单名称不能为空");
            }
            if(CommonUtil.isBlank(menu.getMenuUrl())) {
                return ResultWrapper.getFailureInfo("菜单地址不能为空");
            }
            if(CommonUtil.isNull(menu.getMenuStatus())) {
                menu.setMenuStatus(DefaultStatusEnum.Normal.getType());
            }
            menuService.insert(menu);
            return ResultWrapper.getSuccessInfo("成功保存菜单信息",null);
        } catch (Exception e) {
            log.error("新增菜单异常", e);
        }
        return ResultWrapper.getFailureInfo("新增菜单异常");
    }


}
