package com.springboot.webdemo.service.permission.impl;


import com.springboot.webdemo.core.result.JSONTreeNode;
import com.springboot.webdemo.dao.BaseDao;
import com.springboot.webdemo.dao.permission.IMenuDao;
import com.springboot.webdemo.model.permission.Menu;
import com.springboot.webdemo.service.BaseServiceImpl;
import com.springboot.webdemo.service.permission.IMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author: 余凯
 * @Description: 菜单业务实现
 * @Date: 2017/12/27
 * @Modified by：
 */
@Service
public class MenuServiceImpl extends BaseServiceImpl<Menu, Integer> implements IMenuService {
    private final static Logger log = LoggerFactory.getLogger(MenuServiceImpl.class);
    @Autowired
    private IMenuDao menuDao;


    @Override
    public BaseDao<Menu, Integer> getBaseDao() {
        return menuDao;
    }

    /**
     * @Author: 余凯
     * @Description: 查询菜单树 - 表格
     * @Date: 2017/12/27
     * @Modified by：
     */
    @Override
    public List<JSONTreeNode> findMenuTree(Map<String, Object> condition) {
        List<Menu> menuList = menuDao.queryList(condition, "menu_order_no", "asc");
        List<JSONTreeNode> treeNodeList = new ArrayList<>();
        JSONTreeNode node;
        //拼装成树
        for (Menu menu : menuList) {
            node = new JSONTreeNode();
            node.setId(menu.getMenuId());
            node.setLabel(menu.getMenuName());
            node.setParentId(menu.getMenuParentId());
            if (node.getParentId() == null) {
                node.setParentId(0);
            }
            node.setUseStatus(menu.getMenuStatus());
            node.setOrderNo(menu.getMenuOrderNo());
            treeNodeList.add(node);
        }
        //List<JSONTreeNode> result = Entity2TreeUtils.buildTreeJson(treeNodeList);
        //采用java8来进行数据处理测试

            //先分组
            Map<Integer, List<JSONTreeNode>> groupBy = treeNodeList.stream().collect(Collectors.groupingBy(JSONTreeNode::getParentId));
            //分组后，遍历Map
            Map<Integer, Integer> findMap = new HashMap<>();
            groupBy.keySet().stream().flatMap(m -> groupBy.get(m).stream()).forEach(item -> {
                int findID = item.getId();
                if (groupBy.get(findID) != null) {
                    item.setChildren(groupBy.get(findID));
                    findMap.put(findID, 1);
                }
            });
            //findMap 1:1
            //map转list并且过滤掉已查询的
            List<JSONTreeNode> result = new ArrayList<>();
            groupBy.keySet().stream().filter(key -> findMap.get(key) == null).flatMap(m -> groupBy.get(m).stream()).forEach(item -> {
                result.add(item);
            });


        return result;
    }
}
