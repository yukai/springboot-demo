package com.springboot.webdemo.service.permission;


import com.springboot.webdemo.core.result.JSONTreeNode;
import com.springboot.webdemo.model.permission.Menu;
import com.springboot.webdemo.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * @Author: 余凯
 * @Description: 菜单业务接口
 * @Date: 2017/12/27
 * @Modified by：
 */
public interface IMenuService extends BaseService<Menu,Integer> {
    /**
     * @Author: 余凯
     * @Description: 查询菜单树 - 表格
     * @Date: 2017/12/27
     * @Modified by：
     */
    public List<JSONTreeNode> findMenuTree(Map<String, Object> condition);


}
