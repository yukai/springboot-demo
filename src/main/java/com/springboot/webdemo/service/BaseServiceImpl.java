package com.springboot.webdemo.service;


import com.springboot.webdemo.core.result.Pager;
import com.springboot.webdemo.dao.BaseDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

@Repository
public abstract class BaseServiceImpl<T, PK extends Serializable>  implements BaseService<T, PK> {

    private final static Logger log = LoggerFactory.getLogger(BaseServiceImpl.class);
	
	public abstract BaseDao<T,PK> getBaseDao();
	 /** 
     * 保存单一对象，如果要保存多个对象集合，请参看{@link #insertList(List)} 
     *  
     * @param entity 
     */
    public T insert(T entity){
    	return getBaseDao().insert(entity);
    }
      
    /** 
     * 保存指定类型的对象集合,如果要保存单一对象，请参看{@link #insert(Object)} 
     * @param entities 要保存的数据集合 
     * @return 
     */
    public List<T> insertList(List<T> entities){
    	return getBaseDao().insertList(entities);
    }
  
    /** 
     * 更新对象,如果属性为空，则不会进行对应的属性值更新,如果有属性要更新为null
     *  
     * @param entity 
     *            要更新的实体对象 
     */
    public void update(T entity){
    	getBaseDao().update(entity);
    }

    /**
     * 批量更新
     * @param entities
     */
    @Override
    public void updateList(List<T> entities){
        getBaseDao().updateList(entities);
    }


    /**
     * 清空表内容
     * @author zhangfp
     */
    @Override
    public void delete(){
        getBaseDao().delete();
    }

    /** 
     * 根据id删除对象 
     *  
     * @param id 
     */
    public void deleteById(PK id){
    	getBaseDao().deleteById(id);
    }
  
    /** 
     * 根据条件集合删除对象 
     *  
     * @param condition
     */
    public void deleteByCondition(Map<String, Object> condition){
    	getBaseDao().deleteByCondition(condition);
    }
    
    /** 
     * 批量删除对象 
     *  
     * @param entities
     */
    public void deleteList(List<Integer> entities){
    	getBaseDao().deleteList(entities);
    }

    /**
     * 批量删除对象
     *
     * @param entities
     */
    public void deleteBatch(List<T> entities){
    	getBaseDao().deleteBatch(entities);
    }
	@Override
	public int deleteByIds(PK[] ids) {
		return getBaseDao().deleteByIds(ids);
	}

    /**
     * 根据id进行对象查询 
     *  
     * @param id 
     * @return 
     */
    public T fetch(PK id){
    	return getBaseDao().fetch(id);
    }
  
    /**
     * 根据传入的泛型参数类型查询该类型对应表中的所有数据，返回一个集合对象 
     *  
     * @return 返回泛型参数类型的对象集合，如何取到泛型类型参数，请参看{@link # getEntityClass()}
     */
    public List<T> findAll(){
        BaseDao<T,PK> baseDao = getBaseDao();
        List<T> retList = baseDao.findAll();
    	return retList;
    }
  
    /** 
     * 根据条件集合进行分页查询 
     *  
     * @param condition 
     *            查询条件 
     * @param currentPage 
     *            当前页数 
     * @param pageSize 
     *            页面大小 
     * @return 返回Pager对象 
     */
    public Pager<T> queryPage(Map<String, Object> condition, Integer currentPage,
            Integer pageSize){
    	return getBaseDao().queryPage(condition, currentPage, pageSize);
    }

    /**
     * 分页查询返回结果为Map对象的实现
     *
     * @author zhangfp
     *
     * @param condition
     * @param currentPage
     * @param pageSize
     * @return
     */
    @Override
    public Pager<Map<String,Object>> queryPageMap(Map<String, Object> condition, Integer currentPage, Integer pageSize) {
        Pager<Map<String,Object>> pager = new Pager<Map<String,Object>>(pageSize, count(condition), currentPage);
        return getBaseDao().queryPageMap(condition,currentPage,pageSize);
    }

    /**
     * 条件查询，并提供排序 返回结果为Map
     * @param condition
     *            进行查询的条件集合
     * @param orderBy 排序字段
     * @param sortBy  排序类型（升序或降序）
     * @return
     */
    @Override
    public List<Map<String, Object>> queryListMap(Map<String, Object> condition, String orderBy,
                                                  String sortBy) {
        return getBaseDao().queryListMap(condition,orderBy,sortBy);
    }
    /**
     * 根据条件集合进行指定类型对象集合查询 
     *  
     * @param condition 
     *            进行查询的条件集合 
     * @return 返回泛型参数类型的对象集合，如何取到泛型类型参数，请参看{@link # getEntityClass()}
     */
    public List<T> queryList(Map<String, Object> condition,String orderBy,String sortBy){
    	return getBaseDao().queryList(condition, orderBy, sortBy);
    }
      
    /** 
     * 根据条件集合进行指定类型单一对象查询 
     *  
     * @param condition 
     *            进行查询的条件集合 
     * @return 返回泛型参数类型的对象，如何取到泛型类型参数，请参看{@link # getEntityClass()}，
     */
    public T queryOne(Map<String, Object> condition){
    	return getBaseDao().queryOne(condition);
    }

  
    /** 
     * 根据条件进行数量的查询 
     *  
     * @param condition 
     * @return 返回符合条件的泛型参数对应表中的数量 
     */
    public int count(Map<String, Object> condition){
    	return getBaseDao().count(condition);
    }
    /**
     * 更新或保存，涉及到Mabatis使用的bean只是一个简单的值对象，不能进行id的注解，不知道哪个是主键，所以，必须同时指定t的主键值 
     *  
     * @param t 
     *            要更新或保存的对象 
     * @param id 
     *            要更新或保存的对象的id 
     * @return 返回更新或保存的对象。 
     * @throws NoSuchMethodException  
     * @throws InvocationTargetException  
     * @throws IllegalAccessException  
     * @throws SecurityException  
     * @throws IllegalArgumentException  
     */
    public T updateOrSave(T t, PK id){
    	return getBaseDao().updateOrSave(t, id);
    }


}
