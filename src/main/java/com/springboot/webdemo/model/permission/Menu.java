package com.springboot.webdemo.model.permission;

import java.io.Serializable;

/**
 * @Author: 余凯
 * @Description: 菜单 对应的表为：per_menu
 * @Date: 2017/12/27
 * @Modified by：
 */
public class Menu implements Serializable {
    /**菜单Id*/
    private Integer menuId;
    /**菜单名称*/
    private String menuName;
    /**菜单对应的路由*/
    private String menuUrl;
    /**菜单排序*/
    private Integer menuOrderNo;
    /**菜单状态
     * 1：正常 2：关闭 3：升级中
     * */
    private Integer menuStatus;
    /**菜单类型 1：不展示  2：可展示但不允许进入*/
    private Integer menuType;
    /**上一级菜单*/
    private Integer menuParentId;
    /**系统Id*/
    private Integer sysId;

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public Integer getSysId() {
        return sysId;
    }

    public void setSysId(Integer sysId) {
        this.sysId = sysId;
    }

    public Integer getMenuOrderNo() {
        return menuOrderNo;
    }

    public void setMenuOrderNo(Integer menuOrderNo) {
        this.menuOrderNo = menuOrderNo;
    }

    public Integer getMenuStatus() {
        return menuStatus;
    }

    public void setMenuStatus(Integer menuStatus) {
        this.menuStatus = menuStatus;
    }

    public Integer getMenuParentId() {
        return menuParentId;
    }

    public void setMenuParentId(Integer menuParentId) {
        this.menuParentId = menuParentId;
    }
    public Integer getMenuType() {
        return menuType;
    }

    public void setMenuType(Integer menuType) {
        this.menuType = menuType;
    }
}